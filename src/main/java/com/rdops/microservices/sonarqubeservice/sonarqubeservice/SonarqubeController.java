package com.rdops.microservices.sonarqubeservice.sonarqubeservice;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@RestController
public class SonarqubeController {
	
	@Autowired
	RestTemplate restTemplate;
	
	
	@GetMapping("/hello/{name}")
	public String hello(@PathVariable String name) {
		return "Hello, "+name;
	}
	
	// getSonarqubestatus, checkSonarqubeStatus, sonarGetdata
	@PostMapping("/getSonarqubeStatus")
	@ResponseBody
	public String getSonarqubeStatus(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
		final HttpResponse<JsonNode> response = Unirest.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/components/search?qualifiers=TRK")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// updateSonarqubeUser
	@PostMapping("/updateSonarqubeUser")
	@ResponseBody
	public String updateSonarqubeUser(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
        String newsonarpassword = "admin";
        
		final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/users/change_password?login=" + SonarqubeBean.getUsername() + "&previousPassword=" + SonarqubeBean.getPassword() + "&password=" + newsonarpassword)
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
		
	// sonarqubeProjectDetails
	@PostMapping("/sonarqubeProjectDetails")
	@ResponseBody
	public String sonarqubeProjectDetails(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);
		
        JSONObject sonarqubeData = new JSONObject();
        
		final HttpResponse<JsonNode> sonarqualitygates = Unirest
				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualitygates/project_status?projectId=AWcV5_DQra7uLdlh2QqR")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
//		System.out.println("Quality Gates : "+sonarqualitygates.getBody());
		sonarqubeData.put("quality", sonarqualitygates.getBody().getObject());
		
		final HttpResponse<JsonNode> sonarmetrics = Unirest
				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/measures/component?componentId=AWcV5_DQra7uLdlh2QqR&metricKeys=ncloc,coverage,code_smells,bugs,complexity,violations,line_coverage,new_line_coverage,new_coverage")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
//		System.out.println("Metrics : "+sonarmetrics.getBody());
		sonarqubeData.put("metrics", sonarmetrics.getBody().getObject());
		
		final HttpResponse<JsonNode> sonaranalysis = Unirest
				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/project_analyses/search?project=com.rdops.microservices:jira-service")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
//		System.out.println("Analysis : "+sonaranalysis.getBody());
		sonarqubeData.put("analysis", sonaranalysis.getBody().getObject());
		
		final HttpResponse<JsonNode> sonarduplicate = Unirest
				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/duplications/show?key=com.rdops.microservices:jira-service")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
//		System.out.println("Duplicate : "+sonarduplicate.getBody());
		sonarqubeData.put("duplicate", sonarduplicate.getBody().getObject());
		
		final HttpResponse<JsonNode> sonarissues = Unirest
				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/issues/search")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
//		System.out.println("Issues : "+sonarissues.getBody());
		sonarqubeData.put("issues", sonarissues.getBody().getObject());
		
		final HttpResponse<JsonNode> sonaractivity = Unirest
				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/ce/component?componentId=AWcV5_DQra7uLdlh2QqR")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
//		System.out.println("Activity : "+sonaractivity.getBody());
		sonarqubeData.put("activity", sonaractivity.getBody().getObject());
		
//		final HttpResponse<JsonNode> sonarversion = Unirest
//				.get("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/server/version")
//				.header("Authorization","Basic " + credentialEnc)
//				.asJson();
//		
//		System.out.println("Version : "+sonarversion.getBody());
		System.out.println(sonarqubeData);
		
		return sonarqubeData.toString();
		
	}
	
	// rulesLanguages
	@PostMapping("/rulesLanguages")
	@ResponseBody
	public String rulesLanguages(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/rules/app")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// templateRules
	@PostMapping("/templateRules")
	@ResponseBody
	public String templateRules(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/rules/search?is_template=true&languages=java")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// showRules
	@PostMapping("/showRules")
	@ResponseBody
	public String showRules(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/rules/search?languages=java&ps=500&qprofile=profile&activation=activation")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// getQualityProfiles
	@PostMapping("/getQualityProfiles")
	@ResponseBody
	public String getQualityProfiles(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
			
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/search")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// createQualityProfiles
	@PostMapping("/createQualityProfiles")
	@ResponseBody
	public String createQualityProfiles(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/create?language=java&name=jira")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// qpActivateRules
	@PostMapping("/qpActivateRules")
	@ResponseBody
	public String qpActivateRules(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/activate_rule?rule=1&profile=1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// qpDeactivateRules
	@PostMapping("/qpDeactivateRules")
	@ResponseBody
	public String qpDeactivateRules(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/deactivate_rule?rule=1&profile=1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// addProject
	@PostMapping("/addProject")
	@ResponseBody
	public String addProject(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/add_project?project=1&qualityProfile=1&profile=1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// removeProject
	@PostMapping("/removeProject")
	@ResponseBody
	public String removeProject(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/remove_project?project=1&qualityProfile=1&profile=1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
	// getQpProjects
	@PostMapping("/getQpProjects")
	@ResponseBody
	public String getQpProjects(@RequestBody SonarqubeBean SonarqubeBean) throws IOException, UnirestException, Exception {
		
        byte[] credential = Base64.encodeBase64( (SonarqubeBean.getUsername()+":"+SonarqubeBean.getPassword()).getBytes());
        String credentialEnc = new String(credential);

        final HttpResponse<JsonNode> response = Unirest
				.post("http://"+SonarqubeBean.getSonarqubeip()+":9000/api/qualityprofiles/projects?key=1")
				.header("Authorization","Basic " + credentialEnc)
				.asJson();
		
		System.out.println(response.getBody());
		return response.getBody().toString();
		
	}
	
}
