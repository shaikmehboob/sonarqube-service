package com.rdops.microservices.sonarqubeservice.sonarqubeservice;

public class SonarqubeBean {
	
	public String sonarqubeip;
	public String username;
	public String password;
	
	protected SonarqubeBean() {}

	public SonarqubeBean(String sonarip, String username, String password) {
		super();
		this.sonarqubeip = sonarip;
		this.username = username;
		this.password = password;
	}

	public String getSonarqubeip() {
		return sonarqubeip;
	}

	public void setSonarqubeip(String sonarqubeip) {
		this.sonarqubeip = sonarqubeip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "SonarqubeBean [sonarqubeip=" + sonarqubeip + ", username=" + username + ", password=" + password + "]";
	}
	
}
