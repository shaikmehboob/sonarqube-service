package com.rdops.microservices.sonarqubeservice.sonarqubeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class SonarqubeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonarqubeServiceApplication.class, args);
	}
	
	@Bean
	   public RestTemplate getRestTemplate() {
	      return new RestTemplate();
	   }
}
